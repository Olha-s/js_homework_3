class Employee{
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  get employeeName(){
    return `Name is ${this.name}`;
  }
  set employeeName(newValue){
    this.name = newValue;
  }

  get employeeAge(){
    return `${this.name} is ${this.age} years old`;
  }
  set employeeAge(newValue) {
    this.age = newValue;
  }

  get employeeSalary(){
    return `${this.name} has salary ${this.salary}`;
  }
  set employeeSalary(newValue) {
    this.salary = newValue;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get programmerSalary () {
    super.employeeSalary;
    return (this.salary*3);
  }
}

let programmer1 = new Programmer("Jon", "35", "5000", "UA");
let programmer2 = new Programmer("Max", "30", "2000", "RU");
let programmer3 = new Programmer("Alex", "25", "1500", "EN");
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);

